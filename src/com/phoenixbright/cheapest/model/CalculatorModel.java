package com.phoenixbright.cheapest.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.phoenixbright.cheapest.R;
import com.phoenixbright.cheapest.SettingsActivity;
import com.phoenixbright.cheapest.uom.Uom;

/**
 * Implements the calculation functions.
 * 
 * @author Steve Wamsley <steve@phoenixbright.com>
 */
public class CalculatorModel {

	private Activity _activity;

	private static int _COLOR_DEFAULT = 0x66dddddd;
	private static int _COLOR_WIN = 0x6600aa00;

	/**
	 * Construct model with handle to the activity.
	 * 
	 * @param android
	 *            .app.Activity activity
	 */
	public CalculatorModel(Activity activity) {
		_activity = activity;
	}

	/**
	 * Perform the comparison calculation and update the interface with the
	 * result.
	 * 
	 * @return void
	 */
	public void calculate() {

		double price1Val = getNumericValue(
				(EditText) _activity.findViewById(R.id.editTextPrice1), 0.00);
		double price2Val = getNumericValue(
				(EditText) _activity.findViewById(R.id.editTextPrice2), 0.00);
		double quantity1Val = getNumericValue(
				(EditText) _activity.findViewById(R.id.editTextQuantity1), 1);
		double quantity2Val = getNumericValue(
				(EditText) _activity.findViewById(R.id.editTextQuantity2), 1);
		double size1Val = getNumericValue(
				(EditText) _activity.findViewById(R.id.editTextSize1), 1);
		double size2Val = getNumericValue(
				(EditText) _activity.findViewById(R.id.editTextSize2), 1);

		Spinner units1 = (Spinner) _activity.findViewById(R.id.spinnerUnits1);
		Spinner units2 = (Spinner) _activity.findViewById(R.id.spinnerUnits2);
		Uom selectedUnit1 = (Uom) units1.getItemAtPosition(units1
				.getSelectedItemPosition());
		Uom selectedUnit2 = (Uom) units2.getItemAtPosition(units2
				.getSelectedItemPosition());

		double commonLeft = (size1Val / selectedUnit1.getValue())
				* quantity1Val;
		double commonRight = (size2Val / selectedUnit2.getValue())
				* quantity2Val;

		double cpcLeft = price1Val / commonLeft;
		double cpcRight = price2Val / commonRight;

		double pricePerUnit1 = round(cpcLeft / selectedUnit1.getValue());
		double pricePerUnit2 = round((cpcRight / selectedUnit2.getValue())
				* (selectedUnit2.getValue() / selectedUnit1.getValue()));

		TextView cppu1 = (TextView) _activity
				.findViewById(R.id.textViewComputedCostPerUnit1);
		TextView cppu2 = (TextView) _activity
				.findViewById(R.id.textViewComputedCostPerUnit2);
		
		NumberFormat nf = new DecimalFormat("$#,##0.00");

		cppu1.setText(String.format("%s %s", nf.format(pricePerUnit1),
				selectedUnit1.getName()));
		cppu2.setText(String.format("%s %s", nf.format(pricePerUnit2),
				selectedUnit1.getName()));

		View layoutLeft = (View) _activity.findViewById(R.id.linearLayoutLeft);
		View layoutRight = (View) _activity
				.findViewById(R.id.linearLayoutRight);

		layoutLeft.setBackgroundColor(_COLOR_DEFAULT);
		layoutRight.setBackgroundColor(_COLOR_DEFAULT);

		if (pricePerUnit1 < pricePerUnit2) {

			// left wins!
			layoutLeft.setBackgroundColor(_COLOR_WIN);

		} else if (pricePerUnit2 < pricePerUnit1) {

			// right wins!
			layoutRight.setBackgroundColor(_COLOR_WIN);

		} else {

			// a tie!
			layoutLeft.setBackgroundColor(_COLOR_WIN);
			layoutRight.setBackgroundColor(_COLOR_WIN);

		}

		layoutLeft.invalidate();
		layoutRight.invalidate();

		playSound();
	}

	private double round(double val) {
		BigDecimal bd = new BigDecimal(val);
	    BigDecimal rounded = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
	    return rounded.doubleValue();
	}
	
	private double getNumericValue(EditText text, double defaultValue) {
		double value;
		String theText = text.getText().toString();
		try {
			value = Double.parseDouble(theText);
		} catch (NumberFormatException e) {
			value = defaultValue;
		}
		return value;
	}

	/**
	 * Reset all inputs and results to their default states.
	 * 
	 * @return void
	 */
	public void resetInputs() {

		((EditText) _activity.findViewById(R.id.editTextPrice1)).setText("");
		((EditText) _activity.findViewById(R.id.editTextPrice2)).setText("");
		((EditText) _activity.findViewById(R.id.editTextQuantity1)).setText("");
		((EditText) _activity.findViewById(R.id.editTextQuantity2)).setText("");
		((EditText) _activity.findViewById(R.id.editTextSize1)).setText("");
		((EditText) _activity.findViewById(R.id.editTextSize2)).setText("");

		((Spinner) _activity.findViewById(R.id.spinnerUnits1)).setSelection(0);

		((TextView) _activity.findViewById(R.id.textViewComputedCostPerUnit1))
				.setText("");
		((TextView) _activity.findViewById(R.id.textViewComputedCostPerUnit2))
				.setText("");

		((View) _activity.findViewById(R.id.linearLayoutLeft))
				.setBackgroundColor(_COLOR_DEFAULT);
		((View) _activity.findViewById(R.id.linearLayoutRight))
				.setBackgroundColor(_COLOR_DEFAULT);

	}
	
	private void playSound() {
		
		SharedPreferences sharedPref;
		boolean playSoundPref;
		Context appContext;
		MediaPlayer mp;
		
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this._activity);
		playSoundPref = sharedPref.getBoolean(SettingsActivity.KEY_PREF_PLAY_SOUND, true);
		if (playSoundPref) {
			appContext = _activity.getApplicationContext();
			mp = MediaPlayer.create(appContext, R.raw.chaching);
			mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
				
				@Override
				public void onCompletion(MediaPlayer mp) {
					mp.release();
				}
			});
		}
	}
}
