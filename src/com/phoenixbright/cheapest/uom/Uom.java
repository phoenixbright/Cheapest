package com.phoenixbright.cheapest.uom;

/**
 * Represents a unit of measure which has a group ID, conversion
 * value, and a label / name.
 * 
 * @author Steve Wamsley <steve@phoenixbright.com>
 */
public class Uom {

    private int groupId;
    private double value;
    private String name;

    public Uom(int group_id, double value, String name) {
        super();
        this.groupId = group_id;
        this.value = value;
        this.name = name;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
