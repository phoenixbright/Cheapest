package com.phoenixbright.cheapest.uom;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

/**
 * Parses the unit of measure (uom) XML file.
 * 
 * @author Steve Wamsley <steve@phoenixbright.com>
 */
public class UomXmlParser {

    private static final String ns = null;

    public static String TAG_UNITS = "units";
    public static String TAG_UNIT = "unit";
    public static String ATTR_NAME = "value";
    public static String ATTR_GROUP = "group";
    
    /**
     * Parse the UoM XML resource and return an ordered list of units of measure.
     * 
     * @param java.io.InputStream in the XML input stream
     * 
     * @return List<Uom> the ordered units of meaure
     */
    public List<Uom> parse(InputStream in) {

        List<Uom> entries;
        XmlPullParser parser;
        
        try {
            parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            entries = readFeed(parser);
        } catch (Exception e) {
            entries = new ArrayList<Uom>();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }

        return entries;
    }

    private List<Uom> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Uom> entries = new ArrayList<Uom>();
        String name;
        
        parser.require(XmlPullParser.START_TAG, ns, TAG_UNITS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            name = parser.getName();
            // Starts by looking for the unit tag
            if (name.equals(TAG_UNIT)) {
                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }

        return entries;
    }

    private Uom readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        
        int groupId;
        double value;
        String name;

        parser.require(XmlPullParser.START_TAG, ns, TAG_UNIT);
        
        groupId = Integer.parseInt(parser.getAttributeValue(null, ATTR_GROUP));
        value = Double.parseDouble(parser.getAttributeValue(null, ATTR_NAME));
        name = readText(parser);
        
        parser.require(XmlPullParser.END_TAG, ns, TAG_UNIT);
        
        return new Uom(groupId, value, name);
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                depth--;
                break;
            case XmlPullParser.START_TAG:
                depth++;
                break;
            }
        }
    }
}
