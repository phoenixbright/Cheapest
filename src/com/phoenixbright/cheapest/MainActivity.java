package com.phoenixbright.cheapest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Spinner;

import com.phoenixbright.cheapest.model.CalculatorModel;
import com.phoenixbright.cheapest.uom.Uom;
import com.phoenixbright.cheapest.uom.UomXmlParser;

/**
 * Main activity class for Cheapest application.
 * 
 * @author Steve Wamsley <steve@phoenixbright.com>
 */
public class MainActivity extends Activity implements SensorEventListener {

	/**
	 * Ordered collection of units of measure.
	 */
	private List<Uom> _units = null;

	/*
	 * Stuff for accelerometer detection (shake to clear)
	 */

	private float _lastX;
	private float _lastY;
	private float _lastZ;
	private boolean _initialized;
	private SensorManager _sensorManager;
	private Sensor _accelerometer;
	private final float _NOISE = (float) 22.0;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		final CalculatorModel model;
		Button calculateButton;
		Spinner unitsSpinner;

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		model = new CalculatorModel(this);
		calculateButton = (Button) findViewById(R.id.buttonCalculate);
		calculateButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				model.calculate();
			}
		});

		unitsSpinner = (Spinner) findViewById(R.id.spinnerUnits1);
		unitsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {

				Uom unit;
				Iterator<Uom> iter;
				List<Uom> filteredUnits;
				Spinner spinner;

				unit = (Uom) parent.getItemAtPosition(pos);

				filteredUnits = new ArrayList<Uom>();
				iter = getUnits().iterator();
				while (iter.hasNext()) {
					Uom filterUnit = iter.next();
					if (filterUnit.getGroupId() == unit.getGroupId()) {
						filteredUnits.add(filterUnit);
					}
				}

				spinner = (Spinner) findViewById(R.id.spinnerUnits2);
				addItemsOnUnitsSpinner(spinner, filteredUnits);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});

		addItemsOnUnitsSpinner(unitsSpinner, getUnits());

		/*
		 * Hook up the accelerometer to detect shake
		 */

		_initialized = false;
		_sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		_accelerometer = _sensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		_sensorManager.registerListener(this, _accelerometer,
				SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = false;
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menu_about:
			popupAbout();
			result = true;
			break;
		case R.id.menu_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			result = true;
			break;
		default:
			result = super.onOptionsItemSelected(item);
			break;
		}
		return result;
	}

	private PopupWindow _aboutPopup;

	private void popupAbout() {

		LayoutInflater inflater;
		View layout;
		Button okButton;

		inflater = (LayoutInflater) MainActivity.this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		layout = inflater.inflate(R.layout.activity_about,
				(ViewGroup) findViewById(R.id.about_popup));

		_aboutPopup = new PopupWindow(layout, 460, 640, true);

		// display the popup in the center
		_aboutPopup.showAtLocation(layout, Gravity.CENTER, 0, 0);

		okButton = (Button) layout.findViewById(R.id.buttonOK);
		okButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				_aboutPopup.dismiss();
			}
		});

	}

	private List<Uom> getUnits() {
		if (this._units == null) {
			this._units = new UomXmlParser().parse(getResources()
					.openRawResource(R.raw.uom));
		}
		return this._units;
	}

	private void addItemsOnUnitsSpinner(Spinner spinner, List<Uom> units) {
		ArrayAdapter<Uom> dataAdapter;

		dataAdapter = new ArrayAdapter<Uom>(this,
				android.R.layout.simple_spinner_item, units);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
	}

	protected void onResume() {
		super.onResume();
		_sensorManager.registerListener(this, _accelerometer,
				SensorManager.SENSOR_DELAY_UI);
	}

	protected void onPause() {
		super.onPause();
		_sensorManager.unregisterListener(this);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	@Override
	public void onSensorChanged(SensorEvent event) {

		float x;
		float y;
		float z;
		float deltaX;
		float deltaY;
		float deltaZ;

		x = event.values[0];
		y = event.values[1];
		z = event.values[2];
		if (!_initialized) {
			_lastX = x;
			_lastY = y;
			_lastZ = z;
			_initialized = true;
		} else {
			deltaX = Math.abs(_lastX - x);
			deltaY = Math.abs(_lastY - y);
			deltaZ = Math.abs(_lastZ - z);
			if (deltaX > _NOISE || deltaY > _NOISE || deltaZ > _NOISE) {
				new CalculatorModel(this).resetInputs();
			}
			_lastX = x;
			_lastY = y;
			_lastZ = z;
		}
	}
}
