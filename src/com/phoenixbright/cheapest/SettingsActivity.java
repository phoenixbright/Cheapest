package com.phoenixbright.cheapest;

import android.app.Activity;
import android.os.Bundle;

public class SettingsActivity extends Activity {

	public static String KEY_PREF_PLAY_SOUND = "pref_play_sound";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Display the fragment as the main content.
		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new SettingsFragment()).commit();
	}
}
